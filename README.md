•	Data Mining: Classification model on San Francisco Crime Data (Kaggle Challenge)

Developed this Classification model to classify the category of crime based on the past location and time which can be used by the San Francisco police department to reduce the crime rate. 
The dataset used for the prediction is based on neighborhood of San Francisco crime reports provided by city and county of San Francisco.

The report consist of detail version of code and implementation.
Technologies included: Weka and Tableau.